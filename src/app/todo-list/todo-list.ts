export class TodoList {
  constructor(readonly id: string, public name: string, public items: TodoItem) {}
}
export class TodoItem {
  constructor(public value: string, public checked: boolean = false) {}
}
