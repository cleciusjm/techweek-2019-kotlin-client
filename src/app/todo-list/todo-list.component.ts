import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoList } from './todo-list';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.sass']
})
export class TodoListComponent implements OnInit {
  lists: Observable<TodoList[]>;

  constructor(private http: HttpClient, private snackbar: MatSnackBar) {}

  ngOnInit() {
    this.refresh();
  }

  refresh() {
    this.lists = this.http.get<TodoList[]>('http://localhost:8080/rest/todos');
  }

  remove(list: TodoList) {
    this.http
      .delete(`http://localhost:8080/rest/todos/${list.id}`)
      .subscribe(
        () => this.snackbar.open('Removido com sucesso', null, { duration: 5000 }),
        e => this.snackbar.open('Falha ao remover', null, { duration: 5000 }),
        () => this.refresh()
      );
  }
}
